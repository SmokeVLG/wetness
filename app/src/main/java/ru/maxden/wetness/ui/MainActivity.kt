package ru.maxden.wetness.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ru.maxden.wetness.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}