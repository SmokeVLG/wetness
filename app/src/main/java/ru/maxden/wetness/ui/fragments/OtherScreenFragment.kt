package ru.maxden.wetness.ui.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_other_screen.view.*
import ru.maxden.wetness.BuildConfig
import ru.maxden.wetness.R

class OtherScreenFragment : Fragment() {
    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_other_screen, container, false)
        activity?.title = "Другие расчеты"
        root.tv_author.text =
            "Версия " + BuildConfig.VERSION_NAME + "\n" + "Разработчик Шаплыгин Павел Андреевич" + "\n" + "dfelearn@gmail.com"
        root.тв_другой_описание.text =
            "Разработка мобильных и настольных приложений для инженерных расчетов по буровым растворам. \n" +
                    "Для освоения всех инженерных расчетов по буровым растворам, приглашаем посетить наш сайт \n" +
                    "https://dflearn.ru \n" +
                    " -  Расчеты объемов\n" +
                    " -  Гидродинамическое состояние скважины\n" +
                    " -  Материальные балансы любой сложности\n" +
                    " -  Расчет содержания шлама в любых типах буровых растворов\n" +
                    " -  Расчеты эффективности любых систем очистки\n" +
                    " -  Все расчеты по РУО\n" +
                    " -  Многое другое\n"
        return root
    }

}