package ru.maxden.wetness.ui.fragments
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import kotlinx.android.synthetic.main.fragment_start.view.*
import ru.maxden.wetness.R

class StartFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_start, container, false)
        activity?.title = getString(R.string.main_screen_text)
        root.btn_iffectivnost_systemy_ochstki.setOnClickListener { view ->
            view.findNavController().navigate(R.id.action_startFragment_to_infoFragment)
        }
        root.btn_language.setOnClickListener { view ->
            view.findNavController().navigate(R.id.action_startFragment_to_languageFragment)
        }
        root.btn_vlazhnost_shlama.setOnClickListener { view ->
            view.findNavController().navigate(R.id.action_startFragment_to_calcOptionFragment)
        }
        root.btn_drugie_rascheti.setOnClickListener { view ->
            view.findNavController().navigate(R.id.action_startFragment_to_otherScreenFragment)
        }
        return root
    }
}