package ru.maxden.wetness.ui.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import kotlinx.android.synthetic.main.fragment_calc_option.view.*
import ru.maxden.wetness.R

class CalcOptionFragment : Fragment() {
    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_calc_option, container, false)
        activity?.title = "Вариант расчета"
        root.btn_calc_retort.setOnClickListener { view ->
            view.findNavController()
                .navigate(R.id.action_calcOptionFragment_to_retortMethodFragment)
        }

        root.btn_calc_matbalance.setOnClickListener { view ->
            view.findNavController()
                .navigate(R.id.action_calcOptionFragment_to_matBalanceMethodFragment)
        }
        return root
    }

}