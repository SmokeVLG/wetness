package ru.maxden.wetness.ui.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.blankj.utilcode.util.Utils
import com.itextpdf.text.*
import com.itextpdf.text.pdf.BaseFont
import com.itextpdf.text.pdf.PdfDocument
import com.itextpdf.text.pdf.PdfWriter
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.fragment_retort_method.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.maxden.wetness.R
import java.io.File
import java.io.FileOutputStream

class RetortMethodFragment : Fragment() {
    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_retort_method, container, false)
        activity?.title = "Расчет ретортным методом"
        root.btn_info_calc.setOnClickListener { view ->
            view.findNavController().navigate(R.id.action_typeFluidFragment_to_infoFragment)
        }
        root.btn_print.setOnClickListener {
            Dexter.withActivity(activity)
                .withPermissions(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        if (report.areAllPermissionsGranted()) {
                            val font = Font(
                                BaseFont.createFont(
                                    "assets/fonts/arial.ttf",
                                    BaseFont.IDENTITY_H,
                                    BaseFont.EMBEDDED
                                )
                            )
                            val outPath =
                                context?.getExternalFilesDir(null)
                                    .toString() + "/wetness_report.pdf"
                            val pdf = PdfDocument()
                            val doc = Document(PageSize.A4, 20f, 20f, 20f, 20f)
                            pdf.addWriter(
                                PdfWriter.getInstance(
                                    doc,
                                    FileOutputStream(outPath)
                                )
                            )

                            doc.open()

                            val para1 =
                                "Исходные данные"
                            val paragraph1 = Paragraph(para1, font)
                            paragraph1.alignment = Element.ALIGN_CENTER
                            doc.add(paragraph1)

                            val para2 =
                                "Твердая фаза бурового раствора по реторте, %" + "    -   " + if (root.et_tverdaya_phaza_burovogo_rastvora_po_retorte.text.toString() == "") "0".toFloat()
                                else root.et_tverdaya_phaza_burovogo_rastvora_po_retorte.text.toString()
                                    .toFloat()
                            val paragraph2 = Paragraph(para2, font)
                            paragraph2.alignment = Element.ALIGN_RIGHT
                            doc.add(paragraph2)

                            val para3 =
                                "Плотность фаза влажного шлама по реторте, %" + "    -   " + if (root.et_tverdaya_phaza_vlazhnogo_shlama_po_retorte.text.toString() == "") "0".toFloat()
                                else root.et_tverdaya_phaza_vlazhnogo_shlama_po_retorte.text.toString()
                                    .toFloat()
                            val paragraph3 = Paragraph(para3, font)
                            paragraph3.alignment = Element.ALIGN_RIGHT
                            doc.add(paragraph3)


                            val para15 =
                                "Результаты расчета"
                            val paragraph15 = Paragraph(para15, font)
                            paragraph15.alignment = Element.ALIGN_CENTER
                            doc.add(paragraph15)


                            val para16 =
                                "Коэффициент влажности шлама" + "    -   " + if (root.et_koefficient_vlazhnosti_shlama.text.toString() == "") "0"
                                else root.et_koefficient_vlazhnosti_shlama.text.toString()
                            val paragraph16 = Paragraph(para16, font)
                            paragraph16.alignment = Element.ALIGN_RIGHT
                            doc.add(paragraph16)

                            doc.close()

                            val file = File(outPath)
                            val path: Uri = FileProvider.getUriForFile(
                                requireContext(),
                                context?.packageName + ".provider",
                                file
                            )
                            try {
                                val intent = Intent(Intent.ACTION_VIEW)
                                intent.setDataAndType(path, "application/pdf")
                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                                startActivity(intent)
                            } catch (e: ActivityNotFoundException) {
                                Toast.makeText(
                                    requireContext(),
                                    "There is no PDF Viewer",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        } else {
                            Toast.makeText(
                                requireContext(),
                                "Missing permissions",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permissions: List<PermissionRequest>,
                        token: PermissionToken
                    ) {
                        token.continuePermissionRequest()
                    }
                }).check()


        }
        root.btn_clear.setOnClickListener {
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Внимание!")
                .setMessage("Очистить данные ?")
                .setCancelable(true)
                .setPositiveButton("Очистить") { dialog, id ->
                    root.et_tverdaya_phaza_burovogo_rastvora_po_retorte.setText("")
                    root.et_tverdaya_phaza_vlazhnogo_shlama_po_retorte.setText("")
                    root.et_koefficient_vlazhnosti_shlama.setText("")
                }
            builder.create()
            builder.show();
        }


        root.btn_calc.setOnClickListener {

            if (checkCell(root)) {
                val progress = ProgressDialog(requireContext())
                progress.setTitle("Загрузка")
                progress.setMessage("Расчет параметров...")
                progress.setCancelable(false)

                Utils.runOnUiThread {
                    progress.show()
                }

                GlobalScope.launch(Dispatchers.Default) {
                    var C2 =
                        root.et_tverdaya_phaza_burovogo_rastvora_po_retorte.text.toString()

                    var C3 =
                        root.et_tverdaya_phaza_vlazhnogo_shlama_po_retorte.text.toString()

                    var C4 = 100 - C2.toFloat();
                    var C5 = C4 / C2.toFloat();
                    var C6 = 100 - C3.toFloat();
                    var C7 = C6 / C5;
                    var C8 = C3.toFloat() - C7;
                    var C9 = C7 + C6;
                    var C10 = C9 / C8
                    root.et_koefficient_vlazhnosti_shlama.setText(
                        String.format("%.2f", C10).replace(",", ".")
                    )
                    progress.dismiss();
                }
            }

        }
        return root
    }

    private fun checkCell(root: View): Boolean {

        if (
            root.et_tverdaya_phaza_burovogo_rastvora_po_retorte.text.toString() == ""
            || root.et_tverdaya_phaza_vlazhnogo_shlama_po_retorte.text.toString() == ""

        ) {
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Внимание!")
                .setMessage(
                    "Заполните пустые ячейки в исходных данных!"
                )
                .setCancelable(true)
                .setPositiveButton("Ok") { _, _ ->
                    root.et_koefficient_vlazhnosti_shlama.setText("Ошибка")
                }
            builder.create()
            builder.show()
            return false
        }
        if (


            root.et_tverdaya_phaza_burovogo_rastvora_po_retorte.text.toString().toFloat() < 0
            || root.et_tverdaya_phaza_burovogo_rastvora_po_retorte.text.toString().toFloat() > 100
            || root.et_tverdaya_phaza_vlazhnogo_shlama_po_retorte.text.toString().toFloat() < 0
            || root.et_tverdaya_phaza_vlazhnogo_shlama_po_retorte.text.toString().toFloat() > 100
        ) {
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Внимание!")
                .setMessage(
                    "Твердая фаза должна быть в интервале 0-100%. Проверьте исходные данные!"
                )
                .setCancelable(true)
                .setPositiveButton("Ok") { _, _ ->
                    root.et_koefficient_vlazhnosti_shlama.setText("Ошибка")
                }
            builder.create()
            builder.show()
            return false
        }

        if (
            root.et_tverdaya_phaza_burovogo_rastvora_po_retorte.text.toString().toFloat() >=
            root.et_tverdaya_phaza_vlazhnogo_shlama_po_retorte.text.toString().toFloat()
        ) {
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Внимание!")
                .setMessage(
                    "Твердая фаза бурового раствора должна быть меньше твердой фазы увлажненного шлама. Проверьте исходные данные!"
                )
                .setCancelable(true)
                .setPositiveButton("Ok") { _, _ ->
                    root.et_koefficient_vlazhnosti_shlama.setText("Ошибка")
                }
            builder.create()
            builder.show()
            return false
        }
        return true
    }

}